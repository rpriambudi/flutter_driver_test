import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_driver_test/src/application/keycloak_login/blocs.dart';
import 'package:webview_flutter/webview_flutter.dart';

class KeycloakWebview extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      body: Center(
        child: BlocBuilder<KeycloakBloc, KeycloakState>(
          builder: (context, state) {
            if (state is KeycloakPageInitialized) {
              return WebView(
                initialUrl: state.authorizationUrl,
                javascriptMode: JavascriptMode.unrestricted,
              );
            }
            if (state is KeycloakLoginAuthenticated) {
              return Center(child: Text("Login Successfull"));
            }
            if (state is KeycloakLoginFailed) {
              return Center(child: Text("Login Failed"));
            }

            return Center(child: CircularProgressIndicator());
          }
        ),
      ),
    );
  }
}