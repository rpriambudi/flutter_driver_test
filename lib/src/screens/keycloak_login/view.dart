import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_driver_test/src/application/keycloak_login/blocs.dart';
import 'package:flutter_driver_test/src/screens/keycloak_login/page.dart';

class KeycloakView extends StatelessWidget {
  const KeycloakView();

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => KeycloakBloc()..add(PageRequested()),
        child: KeycloakWebview(),
      ),
    );
  }
}