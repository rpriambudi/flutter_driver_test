import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _panFieldController = TextEditingController();
  final _pinFieldController = TextEditingController();
  final _verifyPinFieldController = TextEditingController();

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      drawer: Drawer(
        key: Key("appDrawer"),
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text("Menu", key: Key("drawerHeaderText")),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text("Menu 1", key: Key("drawerItem1")),
            ),
            ListTile(
              title: Text("Menu 2", key: Key("drawerItem2")),
            ),
          ],
        ),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _panFieldController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Cannot be null';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: "PAN",
                ),
              ),
              TextFormField(
                controller: _pinFieldController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Cannot be null';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: "New PIN",
                ),
              ),
              RaisedButton(
                onPressed: () async {
                  // if (!_formKey.currentState.validate()) {
                  //   Scaffold.of(ctx).showSnackBar(SnackBar(content: Text('Invalid Data'), key: Key("loginFailedSnackbar")));
                  //   return;
                  // }

                  // final String aesKey = "2D4B6150645367566B59703273357638792F423F4528482B4D6251655468576D";
                  // final String encryptedPinblock = HsmPinblock(aesKey: aesKey).encryptClearPin(EncryptPinOptions(pin: _pinFieldController.text));

                  // http.Response response = await http.post(
                  //   "http://192.168.8.101:8080/api/pin/create", 
                  //   headers: <String, String>{"Content-Type": "application/json"}, 
                  //   body: jsonEncode(<String, String>{
                  //     "pin": encryptedPinblock,
                  //     "pan": _panFieldController.text
                  //   })
                  // );

                  // print(response.statusCode);
                  // print(response.body);
                },
                child: Text("Set PIN"),
              ),
              TextFormField(
                controller: _verifyPinFieldController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Cannot be null';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: "PIN",
                ),
              ),
              RaisedButton(
                onPressed: () async {
                  // if (!_formKey.currentState.validate()) {
                  //   Scaffold.of(ctx).showSnackBar(SnackBar(content: Text('Invalid Data'), key: Key("loginFailedSnackbar")));
                  //   return;
                  // }

                  // final String aesKey = "2D4B6150645367566B59703273357638792F423F4528482B4D6251655468576D";
                  // final String encryptedPinblock = HsmPinblock(aesKey: aesKey).ansiFormatZero(FormatZeroOptions(pin: _verifyPinFieldController.text, pan: _panFieldController.text));

                  // print(_verifyPinFieldController.text);

                  // http.Response response = await http.post(
                  //   "http://192.168.8.101:8080/api/pin/verify", 
                  //   headers: <String, String>{"Content-Type": "application/json"}, 
                  //   body: jsonEncode(<String, String>{
                  //     "pin": encryptedPinblock,
                  //     "pan": _panFieldController.text
                  //   })
                  // );

                  // print(response.statusCode);
                  // print(response.body);
                },
                child: Text("Verify PIN"),
              ),
            ],
          ),
        )
      ),
    );
  }
}