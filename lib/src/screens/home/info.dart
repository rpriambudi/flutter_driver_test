import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_driver_test/src/application/person_info/blocs.dart';
import 'package:flutter_driver_test/src/application/person_info/info_bloc.dart';

class PersonInfoPage extends StatelessWidget {
  const PersonInfoPage();

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Person Information"),
      ),
      body: Center(
        child: BlocBuilder<PersonInfoBloc, PersonInfoState>(
          builder: (context, state) {
            if (state is InfoProcessed) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is InfoError) {
              return Center(
                child: Text(state.error),
              );
            }
            if (state is InfoRetrieved) {
              return Center(
                child: ListView.builder(
                  itemCount: state.infoList.length,
                  itemBuilder: (context, index) {
                    final info = state.infoList[index];
                    return ListTile(
                      title: Text(info["name"]),
                      subtitle: Text(info["address"]),
                    );
                  }
                ),
              );
            }
            return Center();
          },
        )
      ),
    );
  }
}