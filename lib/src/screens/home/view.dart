import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_driver_test/src/application/person_info/blocs.dart';

import 'package:flutter_driver_test/src/application/person_info/info_bloc.dart';
import 'package:flutter_driver_test/src/screens/home/info.dart';

class PersonInfoView extends StatelessWidget {
  const PersonInfoView();

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => PersonInfoBloc(hostAddress: "http://192.168.8.101:8080")..add(InfoRequested()),
        child: PersonInfoPage(),
      ),
    );
  }
}