import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_driver_test/src/application/native_login/native_bloc.dart';
import 'package:flutter_driver_test/src/screens/native_login/page.dart';

class NativeLoginView extends StatelessWidget {
  const NativeLoginView();

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => NativeBloc(hostAddress: "http://192.168.8.101:8080"),
        child: NativeLoginForm(),
      ),
    );
  }
}