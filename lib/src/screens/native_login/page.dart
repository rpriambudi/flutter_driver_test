import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_driver_test/src/application/native_login/blocs.dart';
import 'package:flutter_driver_test/src/application/native_login/native_bloc.dart';

class NativeLoginForm extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Native"),
      ),
      body: Center(
        child: BlocBuilder<NativeBloc, NativeState>(
          builder: (context, state) {
            if (state is NativeLoginAuthenticated) {
              return Center(
                child: GestureDetector(
                  onTap: () => Navigator.of(ctx).pushNamed("/dashboard"),
                  child: Text("Login Successful", key: Key("loginSuccessText")),
                )
              );
            }
            if (state is NativeLoginFailed) {
              return Center(child: Text(state.error, key: Key("loginFailedText")));
            }

            return Center(
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      key: Key("usernameFormField"),
                      controller: _usernameController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Cannot be null';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: "Username",
                      ),
                    ),
                    TextFormField(
                      key: Key("passwordFormField"),
                      obscureText: true,
                      controller: _passwordController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Cannot be null';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: "Password",
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: RaisedButton(
                        key: Key("loginFormButton"),
                        onPressed: () {
                          if (!_formKey.currentState.validate()) {
                            Scaffold.of(context).showSnackBar(SnackBar(content: Text('Invalid Data'), key: Key("loginFailedSnackbar")));
                            return;
                          }

                          BlocProvider.of<NativeBloc>(context).add(BasicLoginRequested(username: _usernameController.text, password: _passwordController.text));
                        },
                        child: Text('Login'),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        ),
      ),
    );
  }
}