import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_driver_test/src/router.dart';

class MainView extends StatelessWidget {
  const MainView();

  @override
  Widget build(BuildContext ctx) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Test App"),
        ),
        body: Center(
          child: Row(
            children: [
              RaisedButton(
                key: Key("keycloakLoginButton"),
                onPressed: () => Navigator.of(ctx).pushNamed('/keycloak'),
                child: Text("Keycloak Login"),
              ),
              RaisedButton(
                key: Key("nativeLoginButton"),
                onPressed: () => Navigator.of(ctx).pushNamed('/native'),
                child: Text("Native Login"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class StartView extends StatelessWidget {
  const StartView();

  @override
  Widget build(BuildContext ctx) {
    return BlocProvider(
      create: (_) => null,
      child: MaterialApp(
        onGenerateRoute: AppRouter.generateRoute,
        initialRoute: '/',
      ),
    );
  }
}