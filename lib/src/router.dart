import 'package:flutter/material.dart';

import 'package:flutter_driver_test/src/screens/home/page.dart';
import 'package:flutter_driver_test/src/screens/home/view.dart';
import 'package:flutter_driver_test/src/screens/keycloak_login/pages.dart';
import 'package:flutter_driver_test/src/screens/native_login/pages.dart';
import 'package:flutter_driver_test/src/screens/main/pages.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final Map<String, MaterialPageRoute> routes = {
      '/': MaterialPageRoute(builder: (_) => MainView()),
      '/keycloak': MaterialPageRoute(builder: (_) => KeycloakView()),
      '/native': MaterialPageRoute(builder: (_) => NativeLoginView()),
      '/dashboard': MaterialPageRoute(builder: (_) => HomePage()),
      '/info': MaterialPageRoute(builder: (_) => PersonInfoView()),
    };

    final MaterialPageRoute route = routes[settings.name];
    if (route != null) {
      return route;
    }

    return MaterialPageRoute(
      builder: (_) => Scaffold(
        body: Center(
          child: Text("No suitable route found"),
        )
      )
    );
  }
}