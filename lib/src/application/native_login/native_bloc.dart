import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_driver_test/src/application/native_login/blocs.dart';


class NativeBloc extends Bloc<NativeEvent, NativeState> {
  final String hostAddress;
  final String username = "admin";
  final String password = "password";

  NativeBloc({@required this.hostAddress}): super(NativeInitial());

  @override
  Stream<NativeState> mapEventToState(NativeEvent genericEvent) async* {
    switch (genericEvent.runtimeType) {
      case LoginRequested:
        final LoginRequested event = genericEvent;

        final loginURL = hostAddress + "/mock/login";
        final headers = Map<String, String>();
        if (event.useHeader) {
          headers.putIfAbsent("Device-Type", () => "Android");
        }

        final apiResponse = await http.post(loginURL, headers: headers, body: jsonEncode(<String, String> {
          "username": event.username,
          "password": event.password
        }));

        final Map<String, dynamic> apiResponseBody = jsonDecode(apiResponse.body);
        if (apiResponse.statusCode != 200) {
          yield NativeLoginFailed(error: apiResponseBody["message"]);
          return;
        }

        yield NativeLoginAuthenticated();
        break;
      case BasicLoginRequested:
        final BasicLoginRequested event = genericEvent;

        if (event.username == username && event.password == password) {
          yield NativeLoginAuthenticated();
          return;
        }

        yield NativeLoginFailed(error: "invalid username/password");
        break;
      default:
        yield NativeLoginFailed(error: "Invalid event");
        break;
    }
    
    return;
  }
}