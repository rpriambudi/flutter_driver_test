import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class NativeEvent extends Equatable {
  const NativeEvent();
}

class LoginRequested extends NativeEvent {
  final String username;
  final String password;
  final bool useHeader;

  const LoginRequested({@required this.username, @required this.password, @required this.useHeader}): assert(username != "" && password != "");

  @override
  List<Object> get props => [username, password, useHeader];
}

class BasicLoginRequested extends NativeEvent {
  final String username;
  final String password;

  const BasicLoginRequested({@required this.username, @required this.password}): assert(username != "" && password != ""); 

  @override
  List<Object> get props => [username, password];
}