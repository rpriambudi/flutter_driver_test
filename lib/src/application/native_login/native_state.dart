import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class NativeState extends Equatable {
  const NativeState();

  @override
  List<Object> get props => [];
}

class NativeInitial extends NativeState {}

class NativeLoginProcessed extends NativeState {}

class NativeLoginAuthenticated extends NativeState {}

class NativeLoginFailed extends NativeState {
  final String error;

  NativeLoginFailed({@required this.error});
}
