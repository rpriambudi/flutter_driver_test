import 'package:equatable/equatable.dart';

abstract class SetPinState extends Equatable {
  const SetPinState();

  @override
  List<Object> get props => [];
}

// class 