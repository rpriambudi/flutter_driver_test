import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class PersonInfoState extends Equatable {
  const PersonInfoState();

  @override
  List<Object> get props => [];
}

class InfoInitial extends PersonInfoState {}

class InfoProcessed extends PersonInfoState {}

class InfoRetrieved extends PersonInfoState {
  final List<dynamic> infoList;

  InfoRetrieved({@required this.infoList});
}

class InfoError extends PersonInfoState {
  final String error;

  InfoError({@required this.error});
}