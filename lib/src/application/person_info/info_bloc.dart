import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_driver_test/src/application/person_info/blocs.dart';

class PersonInfoBloc extends Bloc<PersonInfoEvent, PersonInfoState> {
  final String hostAddress;

  PersonInfoBloc({@required this.hostAddress}): super(InfoInitial());

  @override
  Stream<PersonInfoState> mapEventToState(PersonInfoEvent event) async* {
    if (event is InfoRequested) {
      yield InfoProcessed();

      final infoURL = hostAddress + "/mock/data";
      final apiResponse = await http.get(infoURL);
      if (apiResponse.statusCode != 200) {
        yield InfoError(error: "request error");
        return;
      }
      
      final Map<String, dynamic> responseBody = jsonDecode(apiResponse.body);
      final List<dynamic> infoList = responseBody["data"];
      yield InfoRetrieved(infoList: infoList);
    }

    return;
  }
}