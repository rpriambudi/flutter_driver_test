import 'package:equatable/equatable.dart';

abstract class PersonInfoEvent extends Equatable {
  const PersonInfoEvent();
}

class InfoRequested extends PersonInfoEvent {
  const InfoRequested();

  @override
  List<Object> get props => [];
}