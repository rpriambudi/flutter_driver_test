import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class KeycloakState extends Equatable {
  const KeycloakState();

  @override
  List<Object> get props => [];
}

class KeycloakInitial extends KeycloakState {}

class KeycloakPageInitialized extends KeycloakState {
  final String authorizationUrl;

  const KeycloakPageInitialized({@required this.authorizationUrl}): assert(authorizationUrl != null);

  @override
  List<Object> get props => [authorizationUrl];
}

class KeycloakLoginAuthenticated extends KeycloakState {}

class KeycloakLoginFailed extends KeycloakState {}