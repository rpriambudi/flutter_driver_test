import 'package:equatable/equatable.dart';

abstract class KeycloakEvent extends Equatable {
  const KeycloakEvent();
}

class PageRequested extends KeycloakEvent {
  const PageRequested();

  @override
  List<Object> get props => [];
}

class LoginRequested extends KeycloakEvent {
  const LoginRequested();

  @override
  List<Object> get props => [];
}