import 'package:bloc/bloc.dart';

import 'package:flutter_driver_test/src/application/keycloak_login/blocs.dart';
import 'package:flutter_driver_test/src/config/default.dart' as config;
import 'package:flutter_driver_test/src/providers/oauth/oauth.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class KeycloakBloc extends Bloc<KeycloakEvent, KeycloakState> {
  final OAuthProvider _oauthProvider = OAuthProvider();
  
  KeycloakBloc(): super(KeycloakInitial());
  
  @override
  Stream<KeycloakState> mapEventToState(KeycloakEvent event) async* {
    if (event is PageRequested) {
      _oauthProvider.initializeGrant();
      yield KeycloakPageInitialized(authorizationUrl: _oauthProvider.getAuthorizationUrl());
    }

    if (event is LoginRequested) {
      final webview = FlutterWebviewPlugin();
      bool authenticationSuccess = false;

      if (webview  != null) {
        await for (String url in webview.onUrlChanged) {
          if (!url.contains(config.authCallbackUrl)) {
            continue;
          }

          final codeUri = Uri.parse(url);
          if (codeUri.queryParameters['code'] == null) {
            continue;
          }

          await _oauthProvider.setClientFromAuthorizationCode(codeUri.queryParameters['code']);
          authenticationSuccess = true;
          webview.dispose();
          break;
        }
      }

      if (authenticationSuccess) {
        yield KeycloakLoginAuthenticated();
      } else {
        yield KeycloakLoginFailed();
      }
    }
  }
}