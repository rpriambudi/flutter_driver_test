import 'package:flutter_driver_test/src/config/default.dart' as config;
import 'package:oauth2/oauth2.dart' as oauth2;

class OAuthProvider {
  static final OAuthProvider _oauthProvider = OAuthProvider._internal();
  oauth2.Client _client;
  oauth2.AuthorizationCodeGrant _grant;

  factory OAuthProvider() {
    return _oauthProvider;
  }

  void initializeGrant() {
    final grant = new oauth2.AuthorizationCodeGrant(
      config.authClientId, 
      Uri.parse(config.authEndpointUrl), 
      Uri.parse(config.authTokenUrl), 
      secret: config.authClientSecret
    );

    _grant = grant;
    _client = null;
  }

  void close() {
    if (_client != null) {
      _client.close();
    }
  }

  oauth2.Client getClient() {
    return _client;
  }

  Future<void> setClientFromAuthorizationCode(String code) async {
    if (_grant == null) {
      return;
    }

    if (_client == null) {
      _client = await _grant.handleAuthorizationCode(code);
    }
  }

  String getAuthorizationUrl() {
    return _grant.getAuthorizationUrl(Uri.parse(config.authCallbackUrl)).toString();
  }

  OAuthProvider._internal();
}