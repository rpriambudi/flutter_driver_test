import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver driver;

  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });

  test("Test All Elements in Start Screen is Rendered", () async {
    final keycloakButtonFinder = find.byValueKey("keycloakLoginButton");
    final nativeButtonFinder = find.byValueKey("nativeLoginButton");

    await driver.waitFor(keycloakButtonFinder, timeout: Duration(seconds: 10));
    await driver.waitFor(nativeButtonFinder, timeout: Duration(seconds: 10));
  });

  test("Open Native Login Dialog", () async {
    final keycloakButtonFinder = find.byValueKey("keycloakLoginButton");
    final nativeButtonFinder = find.byValueKey("nativeLoginButton");

    await driver.waitFor(keycloakButtonFinder, timeout: Duration(seconds: 10));
    await driver.waitFor(nativeButtonFinder, timeout: Duration(seconds: 10));

    await driver.tap(nativeButtonFinder, timeout: Duration(seconds: 10));

    final usernameFieldFinder = find.byValueKey("usernameFormField");
    final passwordFieldFinder = find.byValueKey("passwordFormField");

    await driver.waitFor(usernameFieldFinder, timeout: Duration(seconds: 10));
    await driver.waitFor(passwordFieldFinder, timeout: Duration(seconds: 10));
  });

  test("Do Successful Native Login", () async {
    final usernameFieldFinder = find.byValueKey("usernameFormField");
    final passwordFieldFinder = find.byValueKey("passwordFormField");
    final loginButtonFinder = find.byValueKey("loginFormButton");

    await driver.waitFor(usernameFieldFinder, timeout: Duration(seconds: 10));
    await driver.waitFor(passwordFieldFinder, timeout: Duration(seconds: 10));
    await driver.waitFor(loginButtonFinder, timeout: Duration(seconds: 10));

    await driver.tap(usernameFieldFinder, timeout: Duration(seconds: 10));
    await driver.enterText("admin", timeout: Duration(seconds: 10));

    await driver.tap(passwordFieldFinder, timeout: Duration(seconds: 10));
    await driver.enterText("password", timeout: Duration(seconds: 10));

    await driver.tap(loginButtonFinder, timeout: Duration(seconds: 10));

    final loginSuccessTextFinder = find.byValueKey("loginSuccessText");

    await driver.waitFor(loginSuccessTextFinder, timeout: Duration(seconds: 10));

    expect(await driver.getText(loginSuccessTextFinder), "Login Successful");
  });
}
