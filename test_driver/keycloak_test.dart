import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver driver;

  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });

  test("Test All Elements in Start Screen is Rendered", () async {
    final keycloakButtonFinder = find.byValueKey("keycloakLoginButton");
    final nativeButtonFinder = find.byValueKey("nativeLoginButton");

    await driver.waitFor(keycloakButtonFinder, timeout: Duration(seconds: 10));
    await driver.waitFor(nativeButtonFinder, timeout: Duration(seconds: 10));
  });

  test("Open Native Login Dialog", () async {
    final keycloakButtonFinder = find.byValueKey("keycloakLoginButton");
    final nativeButtonFinder = find.byValueKey("nativeLoginButton");

    await driver.waitFor(keycloakButtonFinder, timeout: Duration(seconds: 10));
    await driver.waitFor(nativeButtonFinder, timeout: Duration(seconds: 10));

    await driver.tap(keycloakButtonFinder, timeout: Duration(seconds: 10));

    final usernameFieldFinder = find.text("username");
    // final passwordFieldFinder = find.byValueKey("passwordFormField");

    await driver.waitFor(usernameFieldFinder, timeout: Duration(seconds: 10));
    // await driver.waitFor(passwordFieldFinder, timeout: Duration(seconds: 10));
  });
}